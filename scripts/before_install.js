//#!/usr/bin/env node

module.exports = function(ctx) {

    var npm = ctx.requireCordovaModule('npm'),
        fs = ctx.requireCordovaModule('fs'),
        path = ctx.requireCordovaModule('path'),
        deferral = ctx.requireCordovaModule('q').defer();

    npm.load({ 'save-dev': true }, function(err) {
        // handle errors
        if (err) console.error(err);


        // Instala las dependencias de los plugins/hooks/gulp que no están ya instaladas
        var plugins = ['fs-extra', 'gulp-util', 'require-dir', 'gulp-load-plugins', 'minimist', 'gulp-inject',
            'xml2js'];

        npm.commands.list(function(er, plugin) {
            // log errors or data
            var pluginsInstalled = Object.keys(plugin.dependencies);

            plugins.forEach(function (pluginName) {
                // Instalar si hace falta
                if (pluginName in pluginsInstalled === undefined) {
                    console.log('[cordova-utils-ideable]', 'Instalando plugin ->', pluginName);
                    npm.commands.install([pluginName], function(er, data) {
                        // log errors or data
                        console.error(er);
                    });
                }
            });
        });

        npm.on('log', function(message) {
            // log installation progress
            console.log(message);
        });
    });

    return deferral.promise;
};