# Cordova-utils

Funciones y tareas comunes entre los proyectos basados en Cordova/Ionic

### Configuración
* Dependencias que hay que instalar en el proyecto
    * `fs-extra gulp-util require-dir gulp-load-plugins minimist gulp-inject` Se instalan automáticamente.

* En el `gulpfile.js` carga las tareas comunes justo después de la carga de dependencias y antes de las tareas propias del proyecto.

        // ENVIRONMENT TASK - INIT

        // OPTIONS
        var requireDir = require('require-dir');
        var minimist = require('minimist');
        var options = gulp.options = minimist(process.argv.slice(2));


        // load .gulp_settings.json
        var task = options._[0]; // only for first task
        var gulpSettings;
        if (fs.existsSync('./gulp/.gulp_settings.json')) {
          gulpSettings = require('./gulp/.gulp_settings.json');
          var defaults = gulpSettings.defaults;
          if (defaults) {
            // defaults present for said task?
            if (task && task.length && defaults[task]) {
              var taskDefaults = defaults[task];
              // copy defaults to options object
              for (var key in taskDefaults) {
                // only if they haven't been explicitly set
                if (options[key] === undefined) {
                  options[key] = taskDefaults[key];
                }
              }
            }
          }
        }

        // environment
        options.env = options.env || 'dev';

        // print options
        if (defaults && defaults[task]) {
          console.log('defaults for task \'' + task + '\': ', defaults[task]);
        }

        // load tasks
        requireDir('./gulp');

        // ENVIRONMENT TASK - END


* Proyecto con tareas Gulp útiles: https://github.com/driftyco/ionic-gulp-tasks
* Webpack config: https://github.com/ModusCreateOrg/ionic-seed