#!/usr/bin/env node

//
// Don't forget to install xml2js using npm
// `$ npm install xml2js`

var fs = require('fs');
var path = require('path');
var xml2js = require('xml2js');
var cwd = process.cwd();

var projectRoot = cwd;
var pkg = require(path.join(projectRoot, 'package.json'));

// Read config.xml
fs.readFile('config.xml', 'utf8', function(err, xml) {
    if(err) {
        return console.log(err);
    }


    // Parse XML to JS Obj
    xml2js.parseString(xml, function (err, config) {
        if(err) {
            return console.log(err);
        }

        // Replace version from package.json
        config['widget']['$']['version'] = pkg.version;

        // Build XML from JS Obj
        var builder = new xml2js.Builder();
        var configXml = builder.buildObject(config);

        // Write config.xml
        fs.writeFile('config.xml', configXml, function(err) {
            if(err) {
                return console.log(err);
            }

            console.log('> Version updated to ' + pkg.version);
        });

    });
});