(function(isNode, isAngular) {
    'use strict';

    var Config = {

        // TODO Constante de ejemplo, borrar
        sampleProperty: {
            one: {
                ejemplo: 10
            },
            two: 'sample'
        }

        // La coma es OBLIGATORIA, separa la configuración común de la configuración por entorno (dev, staging, prod)
        ,

        // gulp environment: injects environment vars
        /*inject-env*/
        /*endinject*/

    };

    if (isAngular) {
        angular.module('app')
            .constant('Config', Config);
    } else if (isNode) {
        module.exports = Config;
    }
})(typeof module !== 'undefined' && module.exports,
    typeof angular !== 'undefined');