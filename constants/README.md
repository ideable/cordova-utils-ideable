# Constants

Módulo para habilitar variables de constantes por entorno

* Entornos disponibles: `dev`, `staging` y `prod`
* Run: `gulp constants --env=prod`
* Run with another file: `gulp constants --constantsFile=other_file.constants.js`

* Resultado en: `www/app/constants.js`
* Importa módulo `Config` en angular
