#!/usr/bin/env node

var fs = require('fs-extra');
var path = require('path');
var shell = require('shelljs');
var cwd = process.cwd();

var projectRoot = cwd;
var paths = {
    projectRoot: projectRoot,
    gulp: path.join(projectRoot, '/gulp'),
    constants: path.join(projectRoot, '/constants')
};

// Create folder if not exists
if (!fs.existsSync(paths.gulp)) {
    console.log('Creating directory: ', paths.gulp);
    fs.mkdirSync(paths.gulp);
}

// Copy files
if (fs.existsSync(paths.constants)) {
    console.warn('[Constants]', 'Archivos "constants" ya existen!!!');
} else {
    fs.copySync(__dirname + '/../gulp', paths.gulp);

    fs.copySync(__dirname + '/../files', paths.projectRoot);

    console.log('[Constants]', 'Instalado');
    console.log('[Constants]', 'Instrucciones:');
    console.log('[Constants]', 'Leer README.md de cordova-utils-ideable');
    console.log('[Constants]', '> gulp constants');
}
