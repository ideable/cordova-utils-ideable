'use strict';
// gulp
var gulp = require('gulp');
var rename = require('gulp-rename');
var options = gulp.options;
// plugins
var $ = require('gulp-load-plugins')();
var pkg = require('../package.json');

/**
 * transforms object into a string format that is ready for injection at indentation 4
 * @param  {Object} obj Object with values to inject
 * @return {String}     properly formatted string
 */
var injectFormat = function (obj) {
    // indentation of 2
    obj = JSON.stringify(obj, null, 2);
    // replace all doublequotes with singlequotes
    obj = obj.replace(/\"/g, '\'');
    // remove first and last line curly braces
    obj = obj.replace(/^\{\n/, '').replace(/\n\}$/, '');
    // remove indentation of first line
    obj = obj.replace(/^( ){2}/, '');
    // insert padding for all remaining lines
    obj = obj.replace(/\n( ){2}/g, '\n    ');

    return obj;
};

gulp.task('constants', function () {
    var constantsFile = options.constantsFile || 'constants.js';
    if (options.constantsFile) {
        console.log('[cordova-utils-ideable]', '[constants]', 'Using file->', constantsFile);
    }

    return gulp.src('constants/' + constantsFile)
        .pipe(
            $.inject(
                gulp.src('constants/env-' + options.env + '.json'),
                {
                    starttag: '/*inject-env*/',
                    endtag: '/*endinject*/',
                    transform: function (filePath, file) {
                        var json;
                        try {
                            json = JSON.parse(file.contents.toString('utf8'));
                        } catch (e) {
                            console.error(e);
                        }

                        json.version = pkg.version;

                        if (json) {
                            json = injectFormat(json);
                        }

                        return json;
                    }
                }))
        .pipe(rename('constants.js'))
        .pipe(gulp.dest('www/js/'));
});