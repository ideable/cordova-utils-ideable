#!/usr/bin/env node

console.log(' ');
console.log(' ');
console.log('CORDOVA HOOK - after_prepare');
console.log('moving "Notificacion ICON" file to "Android/res"...');

/**
 * After prepare, files are copied to the platforms/ios and platforms/android folders.
 * Lets clean up some of those files that arent needed with this hook.
 */
var path = require('path');
var fs = require('fs-extra');
var cwd = process.cwd();
var projectRoot = cwd;

var androidPlatformDir = path.join(projectRoot, '/platforms/android');
var androidPlatformsDir_res = path.join(projectRoot, '/platforms/android/res');

// When the name is "ic_stat"
var icStat = path.join(projectRoot, '/resources/ic_stat');
if (fs.existsSync(androidPlatformDir) && fs.existsSync(icStat)) {
    console.log("Processing platform - ANDROID");

    fs.copy(icStat, androidPlatformsDir_res, {mkdirp: true}, function (err) {
        if (err) {
            console.error("icStat to Android - ERROR", err);
        } else {
            console.log("icStat to Android - OK");
        }
    });
}

// When the name is "notification_icon"
var notificationIcon = path.resolve(projectRoot, '/resources/notification_icon');
if (fs.existsSync(androidPlatformDir) && fs.existsSync(notificationIcon)) {
    console.log("Processing platform - ANDROID");

    fs.copy(notificationIcon, androidPlatformsDir_res, {mkdirp: true}, function (err) {
        if (err) {
            console.error("notification_icon to Android - ERROR", err);
        } else {
            console.log("notification_icon to Android - OK");
        }
    });


}